from flask import Flask,redirect, url_for, Response, jsonify
from flask import request
from model import db
from model import User
from model import CreateDB
from model import app as application
import simplejson as json
from sqlalchemy.exc import IntegrityError
import os
import httplib


# initate flask app
app = Flask(__name__)

# method for posting data.
@app.route('/v1/expenses', methods= ['POST'])
def post_url():
	try:
		# creating database by calling my CreateDB class in the model class
		CreateDB(hostname = "mysqlserver") 
		# calling create all method to create table in my database according to the model defined.
		db.create_all()

		# gettign the data from the post and storing it in a variable to access it.
		my_data = request.get_json(force=True)
		name = my_data['name']
		email = my_data['email']
		category = my_data['category']
		description = my_data['description']
		link = my_data['link']
		estimated_costs = my_data['estimated_costs']
		submit_date = my_data['submit_date']
		status = "pending"
		decision_date = ""
		
		# store the data in the database
		db_object = User(name,email,category,description,link,estimated_costs,submit_date,status,decision_date)
		db.session.add(db_object)
		db.session.commit()
		
		# sending response back to the user 
		user_obj = User.query.filter_by(email=email).first()
		my_id = user_obj.id
		my_dict={'id':my_id,'name':name,'email':email,'category':category,'description':description,'link':link,'estimated_costs':estimated_costs,'submit_date':submit_date,'status':status,'decision_date':decision_date}
		required_response = jsonify(my_dict)
		required_response.status_code =201

		return required_response
		
	except Exception,e:
		db.session.rollback()
		return str(e)
		
# method for handle the get request
@app.route('/v1/expenses/<expense_id>', methods = ['GET'])
def get_url(expense_id):
	try:
		db.create_all()
		get_object= User.query.filter_by(id=expense_id).all()
		get_dict = {}
		for user in get_object:
			get_dict = {
							'id': user.id,
							'name': user.name,
							'email': user.email,
							'category': user.category,
							'description': user.description,
							'link': user.link,
							'estimated_costs': user.estimated_costs,
							'submit_date': user.submit_date,
							'status': user.status,
							'decision_date': user.decision_date
						    }
		

		if get_dict == {}:
			return Response(status=httplib.NOT_FOUND)
		else:
			get_response = jsonify(get_dict)
			get_response.status_code =200
			return get_response
		
	except Exception,e:
		return str(e)


# method to handle the put requests
@app.route('/v1/expenses/<expense_id>', methods = ['PUT'])
def put_url(expense_id):
	try:
		
		put_data = request.get_json(force=True)
		updated_cost = put_data['estimated_costs']

		update_object = db.session.query(User).filter_by(id=expense_id).update({"estimated_costs":updated_cost})
		db.session.commit()
				
		return Response(status=httplib.ACCEPTED)
	except Exception,e:
		return str(e)	
		
# method to handle the delete 
@app.route('/v1/expenses/<expense_id>', methods = ['DELETE'])
def delete_handler(expense_id):
	try:
		delete_obj = User.query.filter_by(id=expense_id).first()
		db.session.delete(delete_obj)
		db.session.commit()
		return Response(status=httplib.NO_CONTENT)
	except:
		return "Error in the delete handler"
	

 
if __name__ == "__main__":
	app.run(host="0.0.0.0", port=5000, debug=True)

